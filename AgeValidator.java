package bll.validators;

import models.Client;


public class AgeValidator implements Validator<Client> {
	private static final int MIN_AGE = 18;
	private static final int MAX_AGE = 120;

	public void validate(Client t) {

		if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
			throw new IllegalArgumentException("The Customer Age limit is not respected!");
		}

	}

}
