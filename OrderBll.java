package bll;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.NoSuchElementException;


import dao.*;


import models.Order;
import models.Product;


public class OrderBll {

	public OrderBll() {
	}

	public Order findOrderById(int id) {
		Order st = OrderDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Order with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertOrder(Order Order) {
		Product pd = ProductDao.findById(Order.getProductId());
		if(pd.getQuantity()<Order.getQuantity())
		{
			throw new IllegalArgumentException("Under stock");
		}
		else {
				ProductDao.update(pd.getId(),pd.getName() , pd.getPrice(), pd.getQuantity()-Order.getQuantity());
				PrintWriter writer;
				try {
					writer = new PrintWriter("C:\\Users\\Asus\\Desktop\\"+ClientDao.findById(Order.getClientId()).getName()+"_"+Order.getId()+".txt", "UTF-8");
					writer.println("Order issued");
					writer.println("For "+ClientDao.findById(Order.getClientId()).getName());
					writer.println("product:"+pd.getName()+" quantity:"+Order.getQuantity()+" price:"+Order.getQuantity()*pd.getPrice());
					writer.close();
				} catch (FileNotFoundException | UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				return OrderDao.insert(Order);
				
		}
	}
	public void deleteById(int id) {
		Order st = OrderDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Order with id =" + id + " was not found!");
		}
		else
		{
			OrderDao.delete(id);
		}

	}
	public ArrayList<Order> getAll() {
		ArrayList<Order> st = OrderDao.selectAll();
		if (st == null) {
			throw new NoSuchElementException("couldn't get all orders\n");
		}
		return st;
	}
	
}
