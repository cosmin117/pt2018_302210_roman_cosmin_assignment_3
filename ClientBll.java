package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.AgeValidator;
import bll.validators.Validator;
import dao.ClientDao;
import models.Client;


public class ClientBll {

	private List<Validator<Client>> validators;

	public ClientBll() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new AgeValidator());
	}

	public Client findClientById(int id) {
		Client st = ClientDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Client with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertClient(Client Client) {
		for (Validator<Client> v : validators) {
			v.validate(Client);
		}
		return ClientDao.insert(Client);
	}
	public int updateClient(int id,String name,int age, String email) {
		Client client = new Client(id, name, age,email);
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientDao.update(id, name, age,email);
	}
	public void deleteById(int id) {
		Client st = ClientDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Client with id =" + id + " was not found!");
		}
		else
		{
			ClientDao.delete(id);
		}

	}
	public ArrayList<Client> selectAll() {
		ArrayList<Client> st = ClientDao.selectAll();
		if (st == null) {
			throw new NoSuchElementException("couldn't get all clients\n");
		}
		return st;
	}
	
}
