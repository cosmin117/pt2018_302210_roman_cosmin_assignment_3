package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import bll.ClientBll;
import bll.OrderBll;
import bll.ProductBll;
import dao.ClientDao;

import dao.ProductDao;
import models.*;

public class Controller {
	
	private View view;

	public Controller(View view) {
		this.view = view;
		view.i1.addActionListener(new i1Listener());
		view.u1.addActionListener(new u1Listener());
		view.d1.addActionListener(new d1Listener());
		view.i2.addActionListener(new i2Listener());
		view.d2.addActionListener(new d2Listener());
		view.i3.addActionListener(new i3Listener());
		view.u3.addActionListener(new u3Listener());
		view.d3.addActionListener(new d3Listener());
	}

	
	class i1Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			Client c=new Client(0,view.clientName.getText(),Integer.parseInt(view.age.getText()),view.email.getText());
			ClientBll a = new ClientBll();
			a.insertClient(c);
			view.drawCtable();
			
		}

	}
	class u1Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			 ClientBll a = new ClientBll();
			 a.updateClient(Integer.parseInt(view.id1.getText()),view.clientName.getText(),Integer.parseInt(view.age.getText()),view.email.getText());
			 view.drawCtable();
		}
	}
	class d1Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			ClientDao.delete(Integer.parseInt(view.id1.getText()));
			view.drawCtable();
		}
	}
	class i3Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			ProductBll a = new ProductBll();
			Product p=new Product(0,view.productName.getText(),Integer.parseInt(view.prp.getText()),Integer.parseInt(view.qp.getText()));
			a.insertProduct(p);
			view.drawPtable();
		
		}
	}
	class u3Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			ProductBll a = new ProductBll();
			a.updateProduct(Integer.parseInt(view.id3.getText()),view.productName.getText(),Integer.parseInt(view.prp.getText()),Integer.parseInt(view.qp.getText()));
			view.drawPtable();
		}
	}
	class d3Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			ProductDao.delete(Integer.parseInt(view.id3.getText()));
			view.drawPtable();
		}
	}
	
	class i2Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			
			Client client = ClientDao.findById(Integer.parseInt(view.cid.getText()));
			Product product =ProductDao.findById(Integer.parseInt(view.pid.getText()));
			if(client!=null)
			{
				OrderBll o = new OrderBll();
				o.insertOrder(new Order(0,Integer.parseInt(view.qo.getText()),product.getId(),client.getId()));
			}
			view.drawOtable();
		
		}
	}

	class d2Listener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			ProductDao.delete(Integer.parseInt(view.id3.getText()));
			view.drawOtable();
		}
	}

}
