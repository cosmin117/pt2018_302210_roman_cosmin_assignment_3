package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.Client;


public class ClientDao {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
	private final static String findAllStatementString = "SELECT * FROM client";
	private final static String findStatementString = "SELECT * FROM client WHERE id = ?";
	private final static String deleteStatementString = "DELETE FROM client WHERE id = ?";
	private static final String insertStatementString = "INSERT INTO Client (name,age,email)"
			+ " VALUES (?,?,?)";
	private static final String updateStatementString = "UPDATE Client SET name = ?, age = ?, email = ?"
            + " WHERE id = ?";
	
	
	public static Client findById(int id)
	{
		Client client = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, id);
			rs = findStatement.executeQuery();
			rs.next();
			String name = rs.getString("name");
			String email = rs.getString("email");
			int age = rs.getInt("age");
			
			client = new Client(id,name,age,email);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		}
		finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return client;
	}
	
	public static ArrayList<Client> selectAll() {
		ArrayList<Client> list = new ArrayList<Client>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next() != false){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				String email = rs.getString("email");
				list.add(new Client(id, name,age ,email));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return list;
	}
	
	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setInt(2, client.getAge());
			insertStatement.setString(3, client.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	
	
	public static int update(int id, String name,int age,String email) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int updatedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			
			
			updateStatement.setString(1,name);
			updateStatement.setInt(2,age);
			updateStatement.setString(3,email);
			updateStatement.setInt(4,id);
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				updatedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDao:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}
	
	
	
	public static void delete(int id) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		try {	
			
			findStatement = dbConnection.prepareStatement(deleteStatementString);
			findStatement.setInt(1, id);
			findStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDao:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	

}
