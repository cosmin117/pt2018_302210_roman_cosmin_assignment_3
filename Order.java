package models;


public class Order  {
	private int id;
	private int quantity;
	private int productId;
	private int clientId;
	
	public Order()
	{
		
	}
	
	public Order(int id,int quantity,int productId, int clientId)
	{
		this.id=id;
		this.quantity=quantity;
		this.productId=productId;
		this.clientId=clientId;
	}

	public int getId()
	{
		return id;
	}

	public int getQuantity()
	{
		return quantity;
	}
	public int getProductId()
	{
		return productId;
	}
	public int getClientId()
	{
		return clientId;
	}
	public void setId(int id)
	{
		 this.id=id;
	}
	
	public void setQuantity(int quantity)
	{
		 this.quantity=quantity;
	}
	public void setProductId(int productId)
	{
		 this.productId=productId;
	}
	public void setClientId(int clientId)
	{
		 this.clientId=clientId;
	}

}
