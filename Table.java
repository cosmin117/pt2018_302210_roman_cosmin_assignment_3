package presentation;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.*;

public class Table<T> {
	private String[] header;
	private String[][] data;
	public  JTable createTable(ArrayList<T> list)
	{	

		if(list.isEmpty()==false)
		{
			int i=0;
			String s = "";
			for(Field index : list.get(0).getClass().getDeclaredFields())
			{
				s+=index.getName()+" ";
				i++;
			}
			header = s.split(" ");
			data=new String[list.size()][i];
			s="";
	        i=0;
			for(T obj: list)
			{
				int j=0;
				try {
					for(Field index: obj.getClass().getDeclaredFields())
					{
						s="";
						PropertyDescriptor pd = new PropertyDescriptor(index.getName(), obj.getClass());
						Method method = pd.getReadMethod();
						s+=method.invoke(obj);
						data[i][j]=s;
						j++;
					}
				i++;
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
			return new JTable(new DefaultTableModel(data,header));
			
		}
		return null;
		
	}

}
