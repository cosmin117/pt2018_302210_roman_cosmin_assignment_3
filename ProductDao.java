package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.Product;

public class ProductDao {
	
	protected static final Logger LOGGER = Logger.getLogger(ProductDao.class.getName());
	private static final String insertStatementString = "INSERT INTO product (name,price,quantity)"
			+ " VALUES (?,?,?)";
	private static final String updateStatementString = "UPDATE product SET  name = ? , price = ? , quantity = ?"
            + " WHERE id = ?";
	private final static String findStatementString = "SELECT * FROM product WHERE id = ?";
	private final static String deleteStatementString = "DELETE FROM product WHERE id = ?";
	private final static String findAllStatementString = "SELECT * FROM product";
	
	public static Product findById(int id) {
		Product product = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, id);
			rs = findStatement.executeQuery();
			rs.next();
			
			int price = rs.getInt("price");
			int quantity = rs.getInt("quantity");
			String name  = rs.getString("name");
			
			
			product = new Product(id, name, price, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return product;
	}
	public static int insert(Product Product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, Product.getName());
			insertStatement.setInt(2,Product.getPrice());
			insertStatement.setInt(3, Product.getQuantity());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void update(int id, String name ,int price,int quantity) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, name);
			updateStatement.setLong(2, price);
			updateStatement.setLong(3, quantity);
			updateStatement.setInt(4,id);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDao:update" + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		
	}
	public static int delete(int id) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		int deletedId = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();

			ResultSet rs = deleteStatement.getGeneratedKeys();
			if (rs.next()) {
				deletedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDao:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return deletedId;
	}
	
	public static ArrayList<Product> selectAll() {
		ArrayList<Product> list = new ArrayList<Product>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next() != false){			
				int id = rs.getInt("id");
				int price = rs.getInt("price");
				int quantity = rs.getInt("quantity");
				String name = rs.getString("name");
				list.add(new Product(id, name, price, quantity));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return list;
	}
	
	
}
