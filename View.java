package presentation;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import dao.ClientDao;
import dao.OrderDao;
import dao.ProductDao;

import javax.swing.*;


import models.Client;
import models.Order;
import models.Product;
public class View extends JFrame {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel p1,p2,p3;
	private JTabbedPane tab;
	private JLabel cl1,cl2,cl3,cl4,pl1,pl2,pl3,pl4,ol1,ol2,ol3,ol4;
	public JButton i1,u1,d1;
	public JButton i2,d2;
	public JButton i3,u3,d3;
	public JButton cTable,oTable,pTable;
	public JTextField id1,age,clientName,email;
	public JTextField id2,pro,qo,cid,pid;
	public JTextField id3,prp,qp,productName;
	public JScrollPane sp1;
	public JScrollPane sp2;
	public JScrollPane sp3;
	public JTable table1;
	public JTable table2;
	public JTable table3;
	public View()
	{
		setName("Warehouse");
		setSize(800, 600);
		tab = new JTabbedPane();
		p1 = new JPanel();
		p2 = new JPanel();
		p3 = new JPanel();
		i1=new JButton("insert");
		i2=new JButton("insert");
		i3=new JButton("insert");
		u1=new JButton("update");
		
		u3=new JButton("update");
		d1=new JButton("delete");
		d2=new JButton("delete");
		d3=new JButton("delete");
		cl1= new JLabel("id");
		cl2= new JLabel("name");
		cl3= new JLabel("age");
		cl4= new JLabel("email");
		pl1= new JLabel("id");
		pl2= new JLabel("name");
		pl3= new JLabel("price");
		pl4= new JLabel("quantity");
		ol1= new JLabel("id");
		ol2= new JLabel("quantity");
		ol3= new JLabel("clientId");
		ol4= new JLabel("productId");
		
		cTable = new JButton("Table");
		oTable = new JButton("Table");
		pTable = new JButton("Table");
		table1 = null;
		table2 = null;
		table3 = null;
		id1 = new JTextField();
		id2 = new JTextField();
		id3 = new JTextField();
		age = new JTextField();
		clientName =new JTextField();
		email=new JTextField();
		pro=new JTextField();
		qo=new JTextField();
		cid=new JTextField();
		pid=new JTextField();
		prp=new JTextField();
		qp=new JTextField();
		productName=new JTextField();

		id1.setBounds(90,30,90,30);
		id2.setBounds(90,30,90,30);
		id3.setBounds(90,30,90,30);
		
		cl1.setBounds(182,30,90,30);
		cl2.setBounds(182,62,90,30);
		cl3.setBounds(182,94,90,30);
		cl4.setBounds(182,126,90,30);
		pl1.setBounds(182,30,90,30);
		pl2.setBounds(182,62,90,30);
		pl3.setBounds(182,94,90,30);
		pl4.setBounds(182,126,90,30);
		ol1.setBounds(182,30,90,30);
		ol2.setBounds(182,62,90,30);
		ol3.setBounds(182,94,90,30);
		ol4.setBounds(364,30,90,30);
		
		clientName.setBounds(90,62,90,30);
		age.setBounds(90,94,90,30);
		email.setBounds(90,126,90,30);
		qo.setBounds(90,62,90,30);
		cid.setBounds(90,94,90,30);
		pid.setBounds(272,30,90,30);
		prp.setBounds(90,94,90,30);
		qp.setBounds(90,126,90,30);
		productName.setBounds(90,62,90,30);
		i1.setBounds(10, 30, 75, 30);
		u1.setBounds(10, 62, 75, 30);
		d1.setBounds(10, 94, 75, 30);
		cTable.setBounds(10,126,75,30);
		i2.setBounds(10, 30, 75, 30);
		d2.setBounds(10, 62, 75, 30);
		pTable.setBounds(10,126,75,30);
		i3.setBounds(10, 30, 75, 30);
		u3.setBounds(10, 62, 75, 30);
		d3.setBounds(10, 94, 75, 30);
		oTable.setBounds(10,94,75,30);
		p1.add(i1);
		p1.add(u1);
		p1.add(d1);
		p2.add(i2);
		p2.add(d2);
		p3.add(i3);
		p3.add(u3);
		p3.add(d3);
		p1.add(cl1);p1.add(cl2);p1.add(cl3);p1.add(cl4);
		p2.add(ol1);p2.add(ol2);p2.add(ol3);p2.add(ol4);
		p3.add(pl1);p3.add(pl2);p3.add(pl3);p3.add(pl4);
		p1.add(id1);p1.add(age);p1.add(clientName);p1.add(email);p1.add(cTable);
		p2.add(id2);p2.add(pro);p2.add(qo);p2.add(cid);p2.add(pid);p2.add(oTable);
		p3.add(id3);p3.add(prp);p3.add(qp);p3.add(productName);p3.add(pTable);
		
		p1.setVisible(true);
		p2.setVisible(true);
		p3.setVisible(true);
		p1.setLayout(null);
		p2.setLayout(null);
		p3.setLayout(null);
		tab.addTab("customers", p1);
		tab.addTab("orders", p2);
		tab.addTab("products", p3);
		
		
		cTable.addActionListener(new ClientListener());
		pTable.addActionListener(new ProductListener());
		oTable.addActionListener(new OrderListener());
		
		add(tab);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	public void drawCtable()
	{
		
		ArrayList<Client> m= ClientDao.selectAll();
		Table<Client> t=new Table<Client>();
		table1=t.createTable(m);

		table1.setFillsViewportHeight(true);
		sp1 = new JScrollPane(table1);
		sp1.setBounds(10,250,600,125);
		
		p1.add(sp1);
		p1.invalidate();
		p1.validate();
		table1.repaint();
	}

	public void drawPtable()
	{
		
		ArrayList<Product> m= ProductDao.selectAll();
		Table<Product> t=new Table<Product>();
		
		table2=t.createTable(m);
		sp3 = new JScrollPane(table2);
		table2.setFillsViewportHeight(true);
		sp3.setBounds(10,250,600,125);
		p3.add(sp3);
		p3.invalidate();
		p3.validate();
		table2.repaint();
	}
	
	public void drawOtable()
	{
		
		ArrayList<Order> m= OrderDao.selectAll();
		Table<Order> t=new Table<Order>();
		
		table3=t.createTable(m);
		sp2 = new JScrollPane(table3);
		table3.setFillsViewportHeight(true);
		sp2.setBounds(10,250,600,125);
		p2.add(sp2);
		p2.invalidate();
		p2.validate();
		table3.repaint();
	}
	
	
	class ClientListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			
		
			drawCtable();
			
		}
	}
	class ProductListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			
			drawPtable();
				
		
		}
	}
	
	class OrderListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			
			drawOtable();
				
		
		}
	}
	
	

	
}
