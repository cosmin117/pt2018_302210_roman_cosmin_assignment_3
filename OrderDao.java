package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.Order;
public class OrderDao {
	
	protected static final Logger LOGGER = Logger.getLogger(OrderDao.class.getName());
	private static final String insertStatementString = "INSERT INTO Orders (quantity,productId,clientId)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM Orders WHERE id = ?";
	private final static String findAllStatementString = "SELECT * FROM Orders";
	private final static String deleteStatementString = "DELETE FROM Orders WHERE id = ?";
	
	public static Order findById(int orderId) {
		Order order = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderId);
			rs = findStatement.executeQuery();
			rs.next();
			
			int quantity = rs.getInt("quantity");
			int productId = rs.getInt("productId");
			int ClientId = rs.getInt("ClientId");
			
			order = new Order(orderId, quantity, productId, ClientId);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDao:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return order;
	}
	public static int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1,order.getQuantity());
			insertStatement.setInt(2, order.getProductId());
			insertStatement.setInt(3, order.getClientId());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static int delete(int id) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		int deletedId = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			deleteStatement.executeUpdate();

			ResultSet rs = deleteStatement.getGeneratedKeys();
			if (rs.next()) {
				deletedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDao:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return deletedId;
	}
	public static  ArrayList<Order> selectAll() {
		ArrayList<Order> list = new ArrayList<Order>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		
		ResultSet rs = null;
		try {
			
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next() != false){	
				int id = rs.getInt("id");
				int quantity = rs.getInt("quantity");
				int ClientId = rs.getInt("clientId");
				int productId = rs.getInt("productId");
				list.add(new Order(id,quantity,productId,ClientId));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return list;
	}
}
