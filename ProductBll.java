package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;


import dao.ProductDao;

import models.Product;


public class ProductBll {

	public ProductBll() {
	}

	public Product findProductById(int id) {
		Product st = ProductDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertProduct(Product Product) {
		return ProductDao.insert(Product);
	}
	public void updateProduct(int id,String name, int price,int quantity) {
		Product st = ProductDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
		}
		else  ProductDao.update(id, name, price, quantity);
	}
	public void deleteById(int id) {
		Product st = ProductDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
		}
		else
		{
			ProductDao.delete(id);
		}

	}
	public ArrayList<Product> getAll() {
		ArrayList<Product> st = ProductDao.selectAll();
		if (st == null) {
			throw new NoSuchElementException("Could not get all Products\n");
		}
		return st;
	}

}
